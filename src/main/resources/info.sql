-- docker run -d --name mysql.8.0.33 --privileged=true --restart=always -p 3306:3306 -v d:/Soft/MySQL/8.0.33/data:/var/lib/mysql -v d:/Soft/MySQL/8.0.33/config:/etc/mysql/conf.d -v d:/Soft/MySQL/8.0.33/logs:/logs -e MYSQL_ROOT_PASSWORD=vjsp2020 -e TZ=Asia/Shanghai  mysql:8.0.33 --lower-case-table-names=1

-- docker run -d --name mysql.5.7 --privileged=true --restart=always -p 3307:3306 -v d:/Soft/MySQL/5.7.36/data:/var/lib/mysql -v d:/Soft/MySQL/5.7.36/config:/etc/mysql/conf.d -v d:/Soft/MySQL/5.7.36/logs:/logs -e MYSQL_ROOT_PASSWORD=vjsp2020 -e TZ=Asia/Shanghai mysql:5.7 --lower-case-table-names=1

---------------------------------------------------------------------------------------------------------------------------------------
-- 创建数据库
CREATE DATABASE test;

-- 创建表
CREATE TABLE ` config `
(
    `remark` VARCHAR
(
    255
) DEFAULT NULL COMMENT '备注',
    ` TYPE ` SMALLINT DEFAULT NULL COMMENT '类型',
    ` TIME ` datetime
(
    6
) DEFAULT NULL COMMENT '时间'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE =utf8mb4_0900_ai_ci;

-- 添加表字段
ALTER TABLE config
    ADD id BIGINT(20) UNSIGNED NOT NULL auto_increment COMMENT '主键' PRIMARY KEY FIRST;
ALTER TABLE config
    ADD create_by BIGINT(20) UNSIGNED NOT NULL COMMENT '创建人ID';
ALTER TABLE config
    ADD update_by BIGINT(20) UNSIGNED NOT NULL COMMENT '更新人ID';
ALTER TABLE config
    ADD create_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间';
ALTER TABLE config
    ADD update_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间';
ALTER TABLE config
    ADD version INT(10) UNSIGNED DEFAULT 0 NULL COMMENT '版本号';
ALTER TABLE config
    ADD del_flag BIT(1) DEFAULT 0 NULL COMMENT '删除标记（0：正常，1：删除）';

-- 创建视图
CREATE VIEW config_view AS
SELECT *
FROM config;

-- 创建存储过程
DELIMITER //
CREATE PROCEDURE calculate_sum(IN a INT, IN b INT, OUT sum INT, OUT total INT)
BEGIN
    SET sum = a + b;
    SET total = a + b;
END//
DELIMITER;

-- 创建函数
SET GLOBAL log_bin_trust_function_creators = ON;

DELIMITER //
CREATE FUNCTION add_numbers(a INT, b INT) RETURNS INT
BEGIN
    DECLARE result INT;
    SET result = a + b;
    RETURN result;
    END //
DELIMITER ;

    ---------------------------------------------------------------------------------------------------------------------------------

-- 查询数据库表名 接口 tables  无需更改
    SELECT
        TABLE_NAME FROM
	information_schema.TABLES
WHERE
	TABLE_SCHEMA = 'test'
AND
	LOWER(TABLE_NAME) LIKE '%config%'
AND
	TABLE_TYPE = 'BASE TABLE'
ORDER BY
	TABLE_NAME;

-- 查询数据库中View表 接口 views 无需更改
    SELECT
        TABLE_NAME FROM
	information_schema.VIEWS
WHERE
	TABLE_SCHEMA = 'test'
AND
	LOWER(TABLE_NAME) LIKE '%config%'
ORDER BY
	TABLE_NAME;

-- 查询表钟的字段 接口 columns 无需更改
    SELECT
        C.COLUMN_NAME ,
	C.DATA_TYPE,
	IFNULL(C.CHARACTER_MAXIMUM_LENGTH, IFNULL(C.NUMERIC_PRECISION, C.DATETIME_PRECISION)) AS COLUMN_LENGTH,
	C.NUMERIC_SCALE,
	CASE WHEN C.IS_NULLABLE = 'YES' THEN 'TRUE' ELSE 'FALSE' END AS COLUMN_NULLABLE,
	CASE WHEN COLUMN_KEY = 'PRI' THEN 'TRUE' ELSE 'FALSE' END AS COLUMN_PK,
	C.COLUMN_COMMENT,
	C.COLUMN_DEFAULT
FROM
	information_schema.`COLUMNS` C
LEFT JOIN information_schema.`TABLES` T
				ON C.TABLE_SCHEMA = T.TABLE_SCHEMA
				AND C.TABLE_NAME = T.TABLE_NAME
WHERE
	C.TABLE_SCHEMA = 'test'
AND
	T.TABLE_TYPE = 'BASE TABLE'
AND
	LOWER(C.TABLE_NAME) LIKE 'config'
ORDER BY
	C.ORDINAL_POSITION;

    -- 查询数据库中的存储过程 接口 procList

-- 5.7
    SELECT
        NAME FROM
	mysql.proc
WHERE
	DB = 'test'
AND
	TYPE = 'PROCEDURE';

-- 8.0 5.7
    SELECT
        ROUTINE_NAME FROM
	INFORMATION_SCHEMA.ROUTINES
WHERE
	ROUTINE_SCHEMA = 'test'
AND
	ROUTINE_TYPE = 'PROCEDURE' ;

    -- 查询数据库中存储过程参数 接口 procParams

-- 5.7
    SELECT
        CONVERT(PARAM_LIST USING UTF8) AS PARAM_LIST,
	CONVERT(RETURNS USING UTF8) AS FPRETURNS,
	CONVERT(BODY_UTF8 USING UTF8) AS FPBODY
FROM
	mysql.proc
WHERE
	DB = 'test'
AND
	TYPE = 'PROCEDURE'
AND
	LOWER(NAME) = 'calculate_sum';

-- 8.0 5.7
    SELECT
        PARAM_LIST,
	'' AS FPRETURNS,
	ROUTINE_DEFINITION AS FPBODY
FROM
	INFORMATION_SCHEMA.ROUTINES t1
LEFT JOIN (
	SELECT
		GROUP_CONCAT( CONCAT( PARAMETER_MODE, ' ', PARAMETER_NAME, ' ', DATA_TYPE ) SEPARATOR ', ' ) AS PARAM_LIST,
		SPECIFIC_NAME,
		SPECIFIC_SCHEMA
	FROM
		( SELECT PARAMETER_MODE, PARAMETER_NAME, DATA_TYPE, SPECIFIC_NAME, SPECIFIC_SCHEMA FROM information_schema.PARAMETERS WHERE SPECIFIC_SCHEMA = 'test' AND SPECIFIC_NAME = 'calculate_sum' AND ROUTINE_TYPE = 'PROCEDURE' ORDER BY ORDINAL_POSITION ) t11
	) t2
	ON t1.SPECIFIC_NAME = t2.SPECIFIC_NAME  AND t1.ROUTINE_SCHEMA = t2.SPECIFIC_SCHEMA
WHERE
	ROUTINE_SCHEMA = 'test';

    -- 查询数据库中的函数 接口 fincList

-- 5.7
    SELECT
        NAME FROM
	mysql.proc
WHERE
	DB = 'test'
AND
	TYPE = 'FUNCTION';

-- 8.0 5.7
    SELECT
        ROUTINE_NAME FROM
	INFORMATION_SCHEMA.ROUTINES
WHERE
	ROUTINE_SCHEMA = 'test'
AND
	ROUTINE_TYPE = 'FUNCTION' ;

    -- 查询数据库中函数参数 接口 funcParams

-- 5.7
    SELECT
        CONVERT(PARAM_LIST USING UTF8) AS PARAM_LIST,
	CONVERT(RETURNS USING UTF8) AS FPRETURNS,
	CONVERT(BODY_UTF8 USING UTF8) AS FPBODY
FROM
	mysql.proc
WHERE
	DB = 'test'
AND
	TYPE = 'FUNCTION'
AND
	LOWER(NAME) = 'add_numbers';

-- 8.0 5.7
    SELECT
        PARAM_LIST,
	DTD_IDENTIFIER AS FPRETURNS,
	ROUTINE_DEFINITION AS FPBODY
FROM
	INFORMATION_SCHEMA.ROUTINES t1
LEFT JOIN (
						SELECT
							GROUP_CONCAT( CONCAT( PARAMETER_MODE, ' ', PARAMETER_NAME, ' ', DATA_TYPE ) SEPARATOR ', ' ) AS PARAM_LIST,
							SPECIFIC_NAME,
							SPECIFIC_SCHEMA
						FROM
								(
									SELECT
										PARAMETER_MODE,
										PARAMETER_NAME,
										DATA_TYPE,
										SPECIFIC_NAME,
										SPECIFIC_SCHEMA
									FROM
										information_schema.PARAMETERS
									ORDER BY
										ORDINAL_POSITION
								) t11
						GROUP BY
							SPECIFIC_NAME, SPECIFIC_SCHEMA
					) t2
			ON t1.SPECIFIC_NAME = t2.SPECIFIC_NAME  AND t1.ROUTINE_SCHEMA = t2.SPECIFIC_SCHEMA
WHERE
	t1.ROUTINE_SCHEMA = 'test'
AND
	t1.SPECIFIC_NAME = 'add_numbers'
AND
	t1.ROUTINE_TYPE = 'FUNCTION';