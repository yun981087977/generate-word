package indi.bamboo.generate.mapper;

import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @ClassName MysqlTableMapper
 * @Description Mysql数据库表映射器接口
 * @Author Bamboo aspbamboo@gmail.com
 * @Date 2021/4/16 14:23
 * @Version 1.0
 */
public interface MysqlTableMapper extends TableMapper{

    /**
     * 查询所有表名及其说明
     * @param databaseName  数据库名称
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     * @author Bamboo aspbamboo@gmail.com
     * @date 2021/04/16 14:31:23
     */
    @Override
    @Select("SELECT" +
            "  TABLE_NAME AS name, " +
            "  TABLE_COMMENT AS comment " +
            "FROM" +
            "  INFORMATION_SCHEMA.TABLES " +
            "WHERE" +
            "  TABLE_SCHEMA = #{databaseName};")
    List<Map<String, Object>> selectAllTable(String databaseName);

    /**
     * 查询当前表所有字段
     * SELECT * FROM mysql.`INNODB_INDEX_STATS` WHERE table_name = 'table_name'; 查询索引
     * @author Bamboo aspbamboo@gmail.com
     * @date 2018/11/08 17:27:46
     * @param tableName 表名
     * @return java.util.List
     */
    @Override
    @Select("SELECT " +
            "  column_name AS NAME, " +
            "  COLUMN_TYPE AS DATA_TYPE, " +
            "  '' AS DATA_LENGTH, " +
            "  IF(is_nullable = 'YES', '是', '') AS NULLABLE, " +
            "  IF(COLUMN_KEY = 'PRI', '是', '') AS COLUMN_KEY, " +
            "  COLUMN_COMMENT AS COMMENT, " +
            "  column_default AS DEFAULT_VALUE, " +
            "  CASE column_name " +
            "  WHEN 'id' THEN 1 " +
            "  WHEN 'create_by' THEN 3 " +
            "  WHEN 'create_at' THEN 4 " +
            "  WHEN 'update_at' THEN 5 " +
            "  WHEN 'del_flag' THEN 6 " +
            "  WHEN 'version' THEN 7 " +
            "  ELSE 2 END SORT " +
            "FROM " +
            "  information_schema.columns " +
            "WHERE " +
            "  table_name = #{tableName} " +
            "ORDER BY SORT;")
    List<Map<String, String>> selectFields(String tableName);
}