package indi.bamboo.generate.mapper;

import java.util.List;
import java.util.Map;

/**
 * @ClassName TableMapper
 * @Description 数据库表映射器接口
 * @Author Bamboo aspbamboo@gmail.com
 * @date 2018/11/8 0008下午 3:23
 * @Version 1.0
 */
public interface TableMapper {

    /**
     * 查询所有表名及其注释
     * @author Bamboo aspbamboo@gmail.com
     * @date 2018/11/08 15:26:31
     * @param databaseName 数据库名称
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<Map<String, Object>> selectAllTable(String databaseName);

    /**
     * 查询当前表所有字段
     * @author Bamboo aspbamboo@gmail.com
     * @date 2018/11/08 17:27:46
     * @param tableName 表名
     * @return java.util.List
     */
    List<Map<String, String>> selectFields(String tableName);
}