package indi.bamboo.generate.model.enums;

/**
 * @Description 数据库枚举类
 * @Author aspbamboo@gmail.com
 * @Date 2022/8/18 21:04
 * @Version 1.0
 */
public enum DatabaseEnum {

    /**
     * MySQL
     */
    MYSQL("com.mysql.cj.jdbc.Driver", "jdbc:mysql://", "?serverTimezone=Asia/Shanghai"),

    /**
     * Oracle
     */
    ORACLE("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@", ""),

    /**
     * Postgre
     */
    POSTGRE("org.postgresql.Driver", "jdbc:postgresql://", "");

    /**
     * 驱动类
     */
    private final String driverClass;

    /**
     * URL前缀
     */
    private final String urlPrefix;

    /**
     * URL后缀
     */
    private final String urlSuffix;

    DatabaseEnum(String driverClass, String urlPrefix, String urlSuffix) {
        this.driverClass = driverClass;
        this.urlPrefix = urlPrefix;
        this.urlSuffix = urlSuffix;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public String getUrlPrefix() {
        return urlPrefix;
    }

    public String getUrlSuffix() {
        return urlSuffix;
    }
}
